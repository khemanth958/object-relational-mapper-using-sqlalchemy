# Object Relational Mapper Using SQLAlchemy

## Object-relational Mappers (ORMs)
An object-relational mapper (ORM) is a code library that automates the transfer of data stored in relational databases tables into objects that are more commonly used in application code.

# Picture

### Why are ORMs useful?
ORMs provide a high-level abstraction upon a relational database that allows a developer to write Python code instead of SQL to create, read, update and delete data and schemas in their database. Developers can use the programming language they are comfortable with to work with a database instead of writing SQL statements or stored procedures.
For example, without an ORM a developer would write the following SQL statement to retrieve every row in the USERS table where the zip_code column is 94107:

SELECT * FROM USERS WHERE zip_code=94107;

The equivalent Django ORM query would instead look like the following Python code:
/* obtain everyone in the 94107 zip code and assign to users variable */
users = Users.objects.filter(zip_code=94107)

The ability to write Python code instead of SQL can speed up web application development, especially at the beginning of a project. The potential development speed boost comes from not having to switch from Python code into writing declarative paradigm SQL statements.

## SQLAlchemy as the ORM 
SQLAlchemy is a library that facilitates the communication between Python programs and databases. Most of the times, this library is used as an Object Relational Mapper (ORM) tool that translates Python classes to tables on relational databases and automatically converts function calls to SQL statements.

Before diving into the ORM features provided by SQLAlchemy, we need to learn how the core works.

### SQLAlchemy Engines
Whenever we want to use SQLAlchemy to interact with a database, we need to create an Engine.

To create an engine and start interacting with databases, we have to import the create_engine function from the sqlalchemy library and issue a call to it:

from sqlalchemy import create_engine
engine = create_engine('postgresql://usr:pass@localhost:5432/sqlalchemy')

This example creates a PostgreSQL engine to communicate with an instance running locally on port 5432 (the default one). It also defines that it will use usr and pass as the credentials to interact with the sqlalchemy database. Note that, creating an engine does not connect to the database instantly. This process is postponed to when it's needed

### SQLAlchemy Connection Pools
Object pools are used as caches of pre-initialized objects ready to use. That is, instead of spending time to create objects that are frequently needed (like connections to databases) the program fetches an existing object from the pool, uses it as desired, and puts back when done.
There are various implementations of the connection pool pattern available on SQLAlchemy . 
For example, creating an Engine through the create_engine() function usually generates a QueuePool. This kind of pool comes configured with some reasonable defaults, like a maximum pool size of 5 connections.

As usual production-ready programs need to override these defaults.

### SQLAlchemy Dialects
For example, let's say that we want to fetch the first ten rows of a table called people. If our data was being held by a Microsoft SQL Server database engine, SQLAlchemy would need to issue the following query:

SELECT TOP 10 * FROM people;
But, if our data was persisted on MySQL instance, then SQLAlchemy would need to issue:

SELECT * FROM people LIMIT 10;
Therefore, to know precisely what query to issue, SQLAlchemy needs to be aware of the type of the database that it is dealing with. This is exactly what Dialects do.

On its core, SQLAlchemy includes the following list of dialects:
Firebird
Microsoft SQL Server
MySQL
Oracle
PostgreSQL
SQLite
Sybase

### SQLAlchemy Data Types
we use SQLAlchemy data types to map properties of Python classes into columns on a relation database table
class Product(Base):
    __tablename__ = 'products'
    id=Column(Integer, primary_key=True)
    title=Column('title', String(32))
    in_stock=Column('in_stock', Boolean)
    quantity=Column('quantity', Integer)
    price=Column('price', Numeric)

## SQLAlchemy in Flask
https://flask.palletsprojects.com/en/1.1.x/patterns/sqlalchemy/

## Flask-MongoAlchemy
https://pythonhosted.org/Flask-MongoAlchemy/

## References
https://flask-sqlalchemy.palletsprojects.com/en/2.x/
